from nnner import NNNer

TEXT = 'Amazon was founded by Jeff Bezos in 1995. Marc Sloan studied at the Oxford university in 2010. Marc Sloan founded Context Scout in 2015 along with Andy O\'Harney.'
SENTENCE = 'Marc Sloan studied at UCL Academy.'

if __name__ == '__main__':

    ner = NNNer(ner_filename='./ner-gcn-15.tf')

    # Extract all the entities from text (preferred)
    entity_tuples = ner.get_entity_tuples_from_text(TEXT)
    print(entity_tuples)

    # Extract all the entities from a single sentence
    entity_tuples = ner.get_entity_tuples_from_sentence(SENTENCE)
    print(entity_tuples)
    
