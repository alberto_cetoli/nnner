import nnner.utils

from nltk.tokenize import sent_tokenize
from nnner.ner_model import GCNNer


class NNNer:

    def __init__(self, ner_filename):
        self.ner = GCNNer.load(ner_filename)

        
    def _get_entities_from_tuple(self, words, embeddings, tags):
        sentence = ' '.join(words)
        A_fw, A_bw, X = utils.create_graph_from_sentence_and_word_vectors(sentence, embeddings)
        predictions = self.ner.predict(A_fw, A_bw, X)
        entities = [utils.get_entity_name(p) for p in predictions]
        return entities


    def _erase_non_entities(self, all_words, all_entities, all_idx):
        return [(w, e, i) for w, e, i in zip(all_words, all_entities, all_idx) if e]            


    def _join_consecutive_tuples(self, tuples):
        for i in range(len(tuples)-1):
            curr_type = tuples[i][1]
            curr_end_idx = tuples[i][2][1]
            next_type = tuples[i+1][1]
            next_start_idx = tuples[i+1][2][0]
            if curr_type == next_type and curr_end_idx == next_start_idx-1:
                curr_word = tuples[i][0]
                next_word = tuples[i+1][0]
                curr_start_idx = tuples[i][2][0]
                next_end_idx = tuples[i+1][2][1]
                tuples[i+1] = (curr_word + ' ' + next_word,
                               curr_type,
                               [curr_start_idx, next_end_idx])
                tuples[i] = ()
        tuples = [t for t in tuples if t]
        return tuples
    
    
    def _clean_tuples(self, all_words, all_entities, all_idx):
        tuples = self._erase_non_entities(all_words, all_entities, all_idx)
        tuples = self._join_consecutive_tuples(tuples)
        return tuples
    
    
    def get_entity_tuples_from_sentence(self, sentence):
        words, embeddings, tags, idx = utils.get_words_embeddings_tags_from_sentence(sentence)
        entities = self._get_entities_from_tuple(words, embeddings, tags)
        return self._clean_tuples(words, entities, idx)

    
    def get_entity_tuples_from_text(self, text):
        sentences = utils.get_words_embeddings_tags_from_text(text)
        all_words = []
        all_entities = []
        all_idx = []
        for words, embeddings, tags, idx in sentences:
            entities = self._get_entities_from_tuple(words, embeddings, tags)
            all_words.extend(words)
            all_entities.extend(entities)
            all_idx.extend(idx)
        return self._clean_tuples(all_words, all_entities, all_idx)
        
        
        
