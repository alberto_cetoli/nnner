import sys
import tensorflow as tf
import numpy as np

from tensorflow.contrib import rnn


stack_dimension = 2

TINY = tf.constant(1e-6)
ONE = tf.constant(1.)
NAMESPACE = 'gcn'
INPUT_EMBEDDINGS = 160

class GCNNer(object):
    
    def __init__(self):
        with tf.variable_scope(NAMESPACE):
            self.embedding_size = INPUT_EMBEDDINGS
            self.internal_proj_size = 40
            self.memory_dim = 160
            self.vocab_size = 300 + 36
            self.hidden_layer1_size = 160
            self.hidden_layer2_size = 160
            self.hidden_layer3_size = 160
            self.output_size = 16
            self.batch_size = 1
            
            config = tf.ConfigProto(allow_soft_placement=True)
            self.sess = tf.Session(config=config)

            # Bi-LSTM part

            self.enc_inp = tf.placeholder(tf.float32, shape=(None, None, self.vocab_size))
            self.enc_inp_bw = tf.placeholder(tf.float32, shape=(None, None, self.vocab_size))

            self.Wi = tf.Variable(tf.random_uniform([self.vocab_size, self.internal_proj_size],0,0.1))
            self.bi = tf.Variable(tf.random_uniform([self.internal_proj_size],-0.1,0.1))
            self.internal_projection = lambda x: tf.nn.relu(tf.matmul(x, self.Wi) + self.bi)
            
            self.enc_int = tf.map_fn(self.internal_projection, self.enc_inp)
            self.enc_int_bw = tf.map_fn(self.internal_projection, self.enc_inp_bw)
                        
            self.enc_cell_fw = rnn.MultiRNNCell([rnn.GRUCell(self.memory_dim) for _ in range(stack_dimension)], state_is_tuple=True)
            self.enc_cell_bw = rnn.MultiRNNCell([rnn.GRUCell(self.memory_dim) for _ in range(stack_dimension)], state_is_tuple=True)

            with tf.variable_scope('fw'):
                self.encoder_fw, _ = tf.nn.dynamic_rnn(self.enc_cell_fw, self.enc_int, time_major=True, dtype=tf.float32)
            with tf.variable_scope('bw'):
                self.encoder_bw, _ = tf.nn.dynamic_rnn(self.enc_cell_bw, self.enc_int_bw, time_major=True, dtype=tf.float32)

            self.encoder_outputs = tf.concat(values=[self.encoder_fw, self.encoder_bw], axis=2)

            self.Ws = tf.Variable(tf.random_uniform([self.memory_dim*2, self.memory_dim],0,0.1))
            self.bs = tf.Variable(tf.random_uniform([self.memory_dim],-0.1,0.1))
            self.first_projection = lambda x: tf.nn.relu(tf.matmul(x, self.Ws) + self.bs)
            self.hidden = tf.map_fn(self.first_projection, self.encoder_outputs)
        
            # GCN part
            self.DAD_fw = tf.placeholder(tf.float32, shape=(None, None, None), name="DAD_fw")
            self.DAD_bw = tf.placeholder(tf.float32, shape=(None, None, None), name="DAD_bw")
            
            W0_fw = tf.Variable(tf.random_uniform([self.embedding_size, self.hidden_layer1_size], 0, 0.1), name='W0_fw')
            b0_fw = tf.Variable(tf.random_uniform([self.hidden_layer1_size],-0.1,0.1), name='b0_fw')
            self.left_X1_projection_fw = lambda x: tf.matmul(x, W0_fw) + b0_fw
            self.left_X1_fw = tf.map_fn(self.left_X1_projection_fw, self.hidden)
            self.left_X1_fw = tf.transpose(self.left_X1_fw, perm=[1,0,2])
            self.X1_fw = tf.nn.relu(tf.matmul(self.DAD_fw, self.left_X1_fw))
            self.X1_fw = tf.transpose(self.X1_fw, perm=[1,0,2])

            W0_bw = tf.Variable(tf.random_uniform([self.embedding_size, self.hidden_layer1_size], 0, 0.1), name='W0_bw')
            b0_bw = tf.Variable(tf.random_uniform([self.hidden_layer1_size],-0.1,0.1), name='b0_bw')
            self.left_X1_projection_bw = lambda x: tf.matmul(x, W0_bw) + b0_bw
            self.left_X1_bw = tf.map_fn(self.left_X1_projection_bw, self.hidden)
            self.left_X1_bw = tf.transpose(self.left_X1_bw, perm=[1,0,2])
            self.X1_bw = tf.nn.relu(tf.matmul(self.DAD_bw, self.left_X1_bw))
            self.X1_bw = tf.transpose(self.X1_bw, perm=[1,0,2])            

            self.X3 = tf.concat(values=[self.X1_fw, self.X1_bw], axis=2)

            self.Ws = tf.Variable(tf.random_uniform([self.hidden_layer3_size*2, self.hidden_layer3_size],0,0.1), name='Ws')
            self.bs = tf.Variable(tf.random_uniform([self.hidden_layer3_size],-0.1,0.1), name='bs')
            self.first_projection = lambda x: tf.nn.relu(tf.matmul(x, self.Ws) + self.bs)
            self.last_hidden = tf.map_fn(self.first_projection, self.X3)

            self.Wf = tf.Variable(tf.random_uniform([self.hidden_layer3_size, self.output_size],0,0.1), name='Wf')
            self.bf = tf.Variable(tf.random_uniform([self.output_size],-0.1,0.1), name='bf')
            self.final_projection = lambda x: tf.nn.softmax(tf.matmul(x, self.Wf) + self.bf)
            self.outputs = tf.map_fn(self.final_projection, self.last_hidden)
            
            
            # Loss function and training
            self.y_ = tf.placeholder(tf.float32, shape = (None, None, self.output_size))
            self.cross_entropy = tf.reduce_mean(-tf.reduce_sum(self.y_ * tf.log(self.outputs+TINY)
                                                               + (ONE - self.y_) * tf.log(ONE - self.outputs+TINY)
            ))

            # Clipping the gradient
            optimizer = tf.train.AdamOptimizer(1e-4)
            gvs = optimizer.compute_gradients(self.cross_entropy)
            capped_gvs = [(tf.clip_by_value(grad, -1., 1.), var) for grad, var in gvs if var.name.find(NAMESPACE) != -1]
            self.train_step = optimizer.apply_gradients(capped_gvs)
            self.sess.run(tf.global_variables_initializer ())

            
    def _compute_D(self, A):
        num_nodes = A.shape[0]
        D = np.zeros([num_nodes, num_nodes])
        for i in range(num_nodes):
            D[i,i] = 1./(np.sum(A[:,[i]]))
        return D

    
    def _compute_DAD(self, A):
        num_nodes = A.shape[0]
        identity = np.identity(num_nodes)
        A_tilde = identity + A
        D_tilde = self._compute_D(A_tilde)
        DAD = np.dot(np.sqrt(D_tilde),np.dot(A_tilde, np.sqrt(D_tilde)))
        return DAD

    
    def __train (self, A_fw, A_bw, X, y):        
        DAD_fw = np.array([self._compute_DAD(item) for item in A_fw])
        DAD_bw = np.array([self._compute_DAD(item) for item in A_bw])

        X = np.array(X)
        X2 = np.copy(X)

        X = np.transpose(X, (1, 0, 2))
        X2 = np.transpose(X2, (1, 0, 2))
        X2 = X2[::-1, :, :]

        y = np.array(y)
        y = np.transpose(y, (1, 0, 2))
                
        feed_dict = {self.enc_inp: X}
        feed_dict.update({self.enc_inp_bw: X2})

        feed_dict.update({self.DAD_fw: DAD_fw})
        feed_dict.update({self.DAD_bw: DAD_bw})
        feed_dict.update({self.y_: y})
        loss, _, summary = self.sess.run([self.cross_entropy, self.train_step], feed_dict)
        return loss, summary


    def train(self, data, epochs = 20):
        for epoch in range(epochs):
            loss, _ = self.__train ([data[i][0] for i in range(len(data))],
                                    [data[i][1] for i in range(len(data))],
                                    [data[i][2] for i in range(len(data))],
                                    [data[i][3] for i in range(len(data))])
            sys.stdout.flush()


    def __predict(self, A_fw, A_bw, X):
        DAD_fw = np.array([self._compute_DAD(item) for item in A_fw])
        DAD_bw = np.array([self._compute_DAD(item) for item in A_bw])

        X = np.array(X)
        X2 = np.copy(X)
        X = np.transpose(X, (1, 0, 2))
        X2 = np.transpose(X2, (1, 0, 2))
        X2 = X2[::-1, :, :]

        feed_dict = {self.enc_inp: X}
        feed_dict.update({self.enc_inp_bw: X2})
        feed_dict.update({self.DAD_fw: DAD_fw})
        feed_dict.update({self.DAD_bw: DAD_bw})
        y_batch = self.sess.run(self.outputs, feed_dict)
        return y_batch

    
    def predict(self, A_fw, A_bw, X):
        outputs = np.array(self.__predict([A_fw], [A_bw], [X]))
        prediction = []
        for item in outputs:
            item = item [0]
            vector = [0.]*self.output_size
            vector[np.argmax(item)] = 1.
            prediction.append(vector)
        return prediction

    
    def get_embeddings(self, A, X):
        DAD = self._compute_DAD(A)
        X = np.array (X)

        feed_dict = {self.X0: X}
        feed_dict.update({self.DAD: DAD})
        y_batch = self.sess.run(self.embeddings, feed_dict)
        return y_batch[0]



# Loading and saving functions

    def save (self, filename):
        saver = tf.train.Saver()
        saver.save (self.sess, filename)

        
    def load_tensorflow (self, filename):
        saver = tf.train.Saver([v for v in tf.global_variables() if NAMESPACE in v.name])
        saver.restore(self.sess, filename)

        
    @classmethod
    def load(self, filename):
        model = GCNNer()
        model.load_tensorflow(filename)
        return model

