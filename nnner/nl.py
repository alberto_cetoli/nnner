import os
import numpy as np

from spacy.en import English

parser = English()

class SpacyTagger:

    def __init__(self, sentence):
        self.sentence = sentence

class SpacyParser:

    def __init__(self, tagger):
        self.tagger = tagger
        self.parser = parser

    def execute(self, num_nodes=100):
        parsed = self.parser(self.tagger.sentence)
        edges = []
        names = []
        words = []
        tags = []
        types = []
        
        i = 0
        items_dict = dict()
        for item in parsed:
            items_dict[item.idx] = i
            i += 1

        for item in parsed:
            index = items_dict[item.idx]
            for child_index in [items_dict[l.idx] for l in item.children]:
                edges.append((index, child_index))
            names.append("v" + str(index))
            words.append(item.vector)
            tags.append(item.tag_)
            types.append(item.dep_)
        for i in range(len(names),num_nodes):
            names.append("filler_" + str(i))
            words.append(np.zeros(300))
        
        return names[:num_nodes], edges[:num_nodes], words[:num_nodes], tags[:num_nodes], types[:num_nodes]


    def execute_backward(self, num_nodes=100):
        parsed = self.parser(self.tagger.sentence)
        edges = []
        names = []
        words = []
        tags = []
        types = []
        
        i = 0
        items_dict = dict()
        for item in parsed:
            items_dict[item.idx] = i
            i += 1

        for item in parsed:
            index = items_dict[item.idx]
            for child_index in [items_dict[l.idx] for l in item.children]:
                edges.append((child_index, index))
            names.append("v" + str(index))
            words.append(item.vector)
            tags.append(item.tag_)
            types.append(item.dep_)
        for i in range(len(names),num_nodes):
            names.append("filler_" + str(i))
            words.append(np.zeros(300))
        
        return names[:num_nodes], edges[:num_nodes], words[:num_nodes], tags[:num_nodes], types[:num_nodes]
