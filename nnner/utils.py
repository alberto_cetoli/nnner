import spacy
import numpy as np

#parser = spacy.load('en', parser=False, entity=False)
parser = spacy.en.English()

tags = []
tags.append("CC")
tags.append("CD")
tags.append("DT")
tags.append("EX")
tags.append("FW")
tags.append("IN")
tags.append("JJ")
tags.append("JJR")
tags.append("JJS")
tags.append("LS")
tags.append("MD")
tags.append("NN")
tags.append("NNS")
tags.append("NNP")
tags.append("NNPS")
tags.append("PDT")
tags.append("POS")
tags.append("PRP")
tags.append("PRP$")
tags.append("RB")
tags.append("RBR")
tags.append("RBS")
tags.append("RP")
tags.append("TO")
tags.append("UH")
tags.append("VB")
tags.append("VBD")
tags.append("VBG")
tags.append("VBN")
tags.append("VBP")
tags.append("VBZ")
tags.append("WDT")
tags.append("WP")
tags.append("WP$")
tags.append("WRB")

classes = []
classes.append("PERSON")
classes.append("CARDINAL")
classes.append("DATE")
classes.append("TIME")
classes.append("GPE")
classes.append("ORG")
classes.append("NOPR")
classes.append("FAC")
classes.append("LOC")
classes.append("LAW")
classes.append("MONEY")
classes.append("QUANTITY")
classes.append("PERCENT")
classes.append("EVENT")
classes.append("WORK_OF_ART")
classes.append("")


default_vector = parser('entity')[0].vector


def clean_word(word, tag):
    word = word
    if tag == '.':
        word = word.replace('/','')
    return word


def vector_is_empty(vector):
    to_throw = 0
    for item in vector:
        if item == 0.0:
            to_throw += 1
    if to_throw == len(vector):
        return True
    return False


def get_clean_word_vector(word, tag):
    parsed = parser(clean_word(word, tag))
    try:
        vector = parsed[0].vector
        if vector_is_empty(vector):
            vector = default_vector
    except:
        vector = default_vector
    return np.array(vector, dtype=np.float64)


def get_tagging_vector(tag):
    vector = [0.]*(len(tags)+1)
    index = len(tags)
    try:
        index = tags.index(tag)
    except:
        pass
    vector[index] = 1.
    return vector


def get_class_vector(class_name):
    vector = [0.]*(len(classes)+1)
    index = len(classes)
    try:
        index = classes.index(class_name)
    except:
        pass
    vector[index] = 1.
    return vector


def get_entity_num(class_name):
    entity_num = len(classes)
    try:
        entity_num = classes.index(class_name)
    except:
        pass
    return entity_num


def get_vector(vector, tag):
    tag_vector = get_tagging_vector(tag)
    return np.concatenate((vector, tag_vector), axis=0)


def get_words_embeddings_tags_from_sentence(sentence):
    tokens = parser(sentence)
    words = []
    vectors = []
    tags = []
    idx = []
    for token in tokens:
        words.append(token.orth_)
        vectors.append(token.vector)
        tags.append(token.tag_)
        idx.append([token.idx, token.idx+len(token.orth_)])
    return words, vectors, tags, idx


def get_entity_name(prediction):
    index = np.argmax(prediction)
    name = classes[index]
    return name


def get_words_embeddings_tags_from_text(text):
    doc = parser(text)
    sentences = []
    for sent in doc.sents:
        words = []
        vectors = []
        tags = []
        idx = []        
        for token in sent:
            words.append(token.orth_)
            vectors.append(token.vector)
            tags.append(token.tag_)
            idx.append([token.idx, token.idx+len(token.orth_)])
        sentences.append((words, vectors, tags, idx))
    return sentences




def create_graph_from_sentence_and_word_vectors(sentence, word_vectors):
    if not isinstance(sentence, str):
        raise TypeError ("String must be an argument")

    from gensim import utils
    from igraph import Graph
    from nnner.nl import SpacyTagger as Tagger, SpacyParser as Parser

    output_size = 16

    tagger = Tagger(sentence)
    parser = Parser(tagger)

    num_nodes = len(word_vectors)
    
    nodes, edges, words, tags, types = parser.execute(num_nodes=num_nodes)
    g = Graph(directed=True)
    g.add_vertices(nodes)
    g.add_edges(edges)
    A_fw = np.array(g.get_adjacency().data)

    nodes, edges, words, tags, types = parser.execute_backward(num_nodes=num_nodes)
    g2 = Graph(directed=True)
    g2.add_vertices(nodes)
    g2.add_edges(edges)
    A_bw = np.array(g2.get_adjacency().data)

    X = [get_vector(words[i], tags[i]) for i in range(len(words))]
    
    return A_fw, A_bw, X

